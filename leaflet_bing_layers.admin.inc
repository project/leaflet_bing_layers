<?php

/**
 * @file
 * Contains the admin form for the Leaflet Bing Layers module.
 */

/**
 * Page callback for the Leaflet Bing Layers configuration page.
 */
function leaflet_bing_layers_settings_form($form, &$form_state) {
  $form['leaflet_bing_layers_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Bing Maps Key'),
    '#description' => t('Enter a valid <a href="@url">Bing Maps Key</a>.', array('@url' => 'https://msdn.microsoft.com/en-us/library/ff428642.aspx')),
    '#default_value' => variable_get('leaflet_bing_layers_key', ''),
    '#required' => TRUE,
  );

  $map_info = array();
  $enabled_imagery_sets = variable_get('leaflet_bing_layers_enabled', array());

  $form['help_text'] = array(
    '#type' => 'item',
    '#title' => t('Enabled Imagery Sets'),
    '#markup' => t('Use the below table to select which imagery sets will display when the Bing layer is added to a Leaflet map. The imagery set must be enabled in order for it to display. Use the layer name field to customise the label used in the layer switcher and the weight to customise the order.'),
  );

  $form['imagery_sets']['#tree'] = TRUE;
  $form['imagery_sets']['#theme'] = 'leaflet_bing_layers_weight_table';

  foreach (leaflet_bing_layers_get_map_info() as $imagery_set => $info) {
    $default_values = isset($enabled_imagery_sets[$imagery_set]) ? $enabled_imagery_sets[$imagery_set] : array();

    $map_info[$imagery_set] = array(
      'machine_name' => array(
        '#markup' => check_plain($imagery_set),
      ),
      'layer_name' => array(
        '#type' => 'textfield',
        '#title' => t('Layer name'),
        '#title_display' => 'invisible',
        '#default_value' => isset($default_values['layer_name']) ? $default_values['layer_name'] : '',
        '#states' => array(
          'enabled' => array(
            ':input[name="imagery_sets[' . $imagery_set . '][enabled]"]' => array('checked' => TRUE),
          ),
        ),
      ),
      'enabled' => array(
        '#type' => 'checkbox',
        '#title' => t('Enabled'),
        '#title_display' => 'invisible',
        '#default_value' => isset($default_values['enabled']) ? $default_values['enabled'] : 0,
      ),
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#title_display' => 'invisible',
        '#attributes' => array('class' => array('layer-weight')),
        '#default_value' => isset($default_values['weight']) ? $default_values['weight'] : 0,
      ),
    );
  }

  // Sort the imagery sets based on weight then add them to the form.
  uasort($map_info, 'drupal_sort_weight');
  $form['imagery_sets'] += $map_info;

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    ),
  );

  return $form;
}

/**
 * Submit function for the settings form.
 */
function leaflet_bing_layers_settings_form_submit($form, &$form_state) {
  // Save the Bing Maps key.
  $key = $form_state['values']['leaflet_bing_layers_key'];
  variable_set('leaflet_bing_layers_key', $key);

  $imagery_sets = $form_state['values']['imagery_sets'];

  $enabled_imagery_sets = array();
  foreach ($imagery_sets as $imagery_set => $form_values) {
    if ($form_values['enabled']) {
      $enabled_imagery_sets[$imagery_set] = $form_values;
    }
  }

  // Saved the enabled imagery sets.
  variable_set('leaflet_bing_layers_enabled', $enabled_imagery_sets);

  // Clear the Leaflet cache.
  cache_clear_all('leaflet_map_info', 'cache');

  drupal_set_message(t('The configuration options have been saved and the Leaflet map info cache cleared.'));
}

/**
 * Theme function to render the Bing imagery sets in a tabledrag enabled table.
 */
function theme_leaflet_bing_layers_weight_table($variables) {
  $element = $variables['element'];

  $header = array(
    'machine_name' => t('Imagery Set'),
    'layer_name' => t('Layer Name'),
    'enabled' => t('Enabled'),
    'weight' => t('Weight'),
  );

  $rows = array();
  foreach (element_children($element) as $key) {
    $row = array();
    $row['data'] = array();
    foreach ($header as $fieldname => $title) {
      $row['data'][] = drupal_render($element[$key][$fieldname]);
      $row['class'] = array('draggable');
    }
    $rows[] = $row;
  }

  drupal_add_tabledrag('leaflet-bing-layers-map-info', 'order', 'sibling', 'layer-weight');

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'leaflet-bing-layers-map-info'),
  ));
}
