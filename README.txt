INTRODUCTION
------------

This module adds Bing Maps Imagery Sets as an available map style within your
Leaflet map configuration. Through the administration interface you can
configure which Imagery Sets are enabled and displayed, as well as setting a
custom label for display in the layer switcher.

This module uses Bing Maps Layer for Leaflet to fetch and add the Imagery Sets
to the map.

REQUIREMENTS
------------

This module requires the following modules:

 * Libraries API (https://www.drupal.org/project/libraries)
 * Leaflet (https://drupal.org/project/leaflet)

In addition, the Bing Maps Layer for Leaflet JavaScript library uses Promises
which may require a polyfill if support for older browsers is required.

INSTALLATION
------------

* Download the Bing Maps Layer for Leaflet library and unpack into the
  sites/all/libraries directory. Rename the extracted folder leaflet-bing-layer.
* Enable the module and configure the settings at
  /admin/config/system/leaflet-bing-layers
* Add a Leaflet map and select the "Bing Imagery Sets" as the map.

CONFIGURATION
-------------

This module provides a configuration form at
/admin/config/system/leaflet-bing-layers.

MAINTAINERS
-----------

Current maintainers:
 * Hayden Tapp (haydent) - https://www.drupal.org/u/haydent
