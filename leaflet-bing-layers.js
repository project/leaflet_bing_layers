(function ($) {

  Drupal.leaflet._create_layer_orig = Drupal.leaflet.create_layer;

  Drupal.leaflet.create_layer = function(layer, key) {
    if (layer.type === 'bing') {
      var mapLayer = L.tileLayer.bing(layer.options);
      mapLayer._leaflet_id = key;
      mapLayer._type = 'bing';
      return mapLayer;
    }
    // Default to the original code;
    return Drupal.leaflet._create_layer_orig(layer, key);
  };

})(jQuery);